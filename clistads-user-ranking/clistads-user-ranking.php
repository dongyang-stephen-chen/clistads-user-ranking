<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              clistads.com
 * @since             1.0.2
 * @package           Clistads_User_Ranking
 *
 * @wordpress-plugin
 * Plugin Name:       ClistAds User Ranking
 * Plugin URI:        clistads.com
 * Description:       Add User ranking functionality to Clistads
 * Version:           1.0.2
 * Author:            Dongyang Chen
 * Author URI:        clistads.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       clistads-user-ranking
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'CLISTADS_USER_RANKING_VERSION', '1.0.2' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-clistads-user-ranking-activator.php
 */
function activate_clistads_user_ranking() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-clistads-user-ranking-activator.php';
	Clistads_User_Ranking_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-clistads-user-ranking-deactivator.php
 */
function deactivate_clistads_user_ranking() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-clistads-user-ranking-deactivator.php';
	Clistads_User_Ranking_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_clistads_user_ranking' );
register_deactivation_hook( __FILE__, 'deactivate_clistads_user_ranking' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-clistads-user-ranking.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_clistads_user_ranking() {

	$plugin = new Clistads_User_Ranking();
	$plugin->run();

}
run_clistads_user_ranking();
