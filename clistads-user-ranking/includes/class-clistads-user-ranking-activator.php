<?php

/**
 * Fired during plugin activation
 *
 * @link       clistads.com
 * @since      1.0.0
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Ranking_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		self::createDatabases();
	}

	private static function createDatabases(){
		Logger::debug("createDatabases", __FILE__);
		global $wpdb;
		$charset_collate = $wpdb->get_charset_collate();
		/* unique user_votable table */
		$table_name = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTABLE_TABLE;
		$sql = "CREATE TABLE $table_name (
			id bigint unsigned not null primary key auto_increment,
			user_id bigint unsigned not null,
			code varchar(26) not null
		) $charset_collate;";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		Logger::debug($sql);
		$output = dbDelta($sql);
		Logger::debug(print_r($output));

		/* unique user_vote table */
		$table_name = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTE_TABLE;
		$sql = "CREATE TABLE $table_name (
			id bigint unsigned not null primary key auto_increment,
			user_id bigint unsigned not null,
			user_votable_id bigint unsigned not null,
			score tinyint unsigned not null
		) $charset_collate;";
		dbDelta($sql);
	}


}
