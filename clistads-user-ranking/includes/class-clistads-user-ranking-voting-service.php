<?php

class Clistads_User_Ranking_Voting_Service
{
    private Clistads_User_Ranking_Database_Model $dbModel;

    public function __construct($dbModel)
    {
        $this->dbModel = $dbModel;
    }

    public function vote_action(int $ratingUserId, int $ratedUserId, string $ratedItem, int $score){
        if ($this->dbModel->Query_User_Vote($ratingUserId, $ratedUserId, $ratedItem)){
            Logger::info("Updating existing vote: on [$ratedUserId]'s [$ratedItem] with score $score for userId $ratingUserId");
            $this->dbModel->Update_User_Vote($score, $ratingUserId, $ratedUserId, $ratedItem);
        }
        Logger::info("Inserting new vote: on [$ratedUserId]'s [$ratedItem] with score $score for userId $ratingUserId");
        $this->dbModel->Insert_user_vote($ratingUserId, $score, $ratedUserId, $ratedItem);
    }

    public function get_score_for_votable(int $ratedUserId, string $ratedItem){
        $scores_array = $this->dbModel->Query_User_Vote(null, $ratedUserId, $ratedItem);

    }

    public function get_score_for_user(int $ratedUserId){
        $scores_array = $this->dbModel->Query_User_Vote(null, $ratedUserId);
    }

}
