<?php

/**
 * Fired during plugin deactivation
 *
 * @link       clistads.com
 * @since      1.0.0
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/includes
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Ranking_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
