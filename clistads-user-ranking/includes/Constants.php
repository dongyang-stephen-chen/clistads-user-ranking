<?php

define("CLISTADS_USER_RANKING_POST_TYPE", "User-Ranking");
define("CLISTADS_USER_RANKING_META_KEY_RATED_USER", "Rated-User");
define("CLISTADS_USER_RANKING_META_KEY_RATED_USER_VALUE", "Rated-User-Value");
define("CLISTADS_USER_RANKING_USER_META_SCORE_WEIGHT", "Rated-User-Weight");
define("CLISTADS_USER_RANKING_DEFAULT_VOTE_COUNT", 50);
define("CLISTADS_USER_RANKING_DEFAULT_VOTE_SCORE", 5);
define("CLISTADS_USER_RANKING_INCREASE_SCORE_PRODUCT_ID", null); // set the woocommerce product id for the product that is used to buy more score.
define('CLISTADS_USER_RANKING_SCORE_TOO_LOW_PAGE', "");
define('CLISTADS_USER_RANKING_USER_FORCE_LOW_SCORE_ON',null); // force low scores on userID's for testing
define('CLISTADS_USER_RANKING_USER_FORCE_VERY_LOW_SCORE_ON',null);// force low scores on userID's for testing
define('CLISTADS_USER_RANKING_USER_VOTABLE_TABLE',"USER_VOTABLE");// unique user table for storing users that can vote
define('CLISTADS_USER_RANKING_USER_VOTE_TABLE',"USER_VOTE");// unique vote table for storing users' votes