<?php

class Clistads_User_Ranking_Database_Model
{

    public function Insert_user_votable(int $userId, string $ratedItem)
    {
        global $wpdb;
        $table = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTABLE_TABLE;
        $data = array('user_id' => $userId, 'code' => $ratedItem);
        $format = array('%d', '%s');
        $wpdb->insert($table, $data, $format);
        return $wpdb->insert_id;
    }

    /**
     * Insert user vote. Can either provide:
     * 1. $userVotableId
     * 2. $ratedUserId + $ratedItem
     *
     * @param integer $ratingUserId
     * @param integer $score
     * @param integer|null $ratedUserId
     * @param string|null $ratedItem
     * @param integer|null $userVotableId
     * @return int|void id of inserted entry on user_vote table
     */
    public function Insert_user_vote(int $ratingUserId, int $score, int $ratedUserId=null, string $ratedItem=null, int $userVotableId=null)
    {
        Logger::debug("Update_User_Vote", __FILE__);
        global $wpdb;
        $table2 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTE_TABLE;
        if(!$userVotableId){
            $table1 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTABLE_TABLE;
            $query = $wpdb->prepare("SELECT id FROM $table1 WHERE user_id = %d AND code = %s", $ratedUserId, $ratedItem);
            $userVotableId = $wpdb->get_results($query)[0]->id; 
        }
        $data = array('user_id' => $ratingUserId, 'user_votable_id' => $userVotableId, 'score' => $score);
        $format = array('%d', '%d', '%d');
        $wpdb->insert($table2, $data, $format);
        return $wpdb->insert_id;
    }

    /**
     * Query User Vote. Can either provide:
     * 1. $userVoteId
     * 2. $ratingUserId + $ratedUserId + $ratedItem
     *
     * @param integer|null $ratingUserId
     * @param integer|null $ratedUserId
     * @param string|null $ratedItem
     * @param integer|null $userVoteId
     * @return object|void returned search for user's votes
     */
    public function Query_User_Vote(int $ratingUserId=null, int $ratedUserId=null, string $ratedItem=null, int $userVoteId=null)
    {
        global $wpdb;
        $table1 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTABLE_TABLE;
        $table2 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTE_TABLE;
        if ($userVoteId){
            return  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table2 WHERE id = %d", $userVoteId));
        }elseif($ratingUserId){
                return  $wpdb->get_results($wpdb->prepare("SELECT * FROM $table2 WHERE user_id = %d AND user_votable_id = (SELECT id FROM $table1 WHERE user_id = %d AND code = %s)", $ratingUserId, $ratedUserId, $ratedItem));
            }elseif($ratedItem){
                $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table2 WHERE user_votable_id = (SELECT id FROM $table1 WHERE user_id = %d AND code = %s)", $ratedUserId, $ratedItem));
                return  array_column($results, 'score');
            }else{
                $results = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table2 WHERE user_votable_id in (SELECT id FROM $table1 WHERE user_id = %d)", $ratedUserId));
                return  array_column($results, 'score');
            }
            
    }

    /**
     * Update User Vote. Can either provide:
     * 1. $userVoteId
     * 2. $ratingUserId + $ratedUserId + $ratedItem
     *
     * @param integer $score
     * @param integer|null $ratingUserId
     * @param integer|null $ratedUserId
     * @param string|null $ratedItem
     * @param integer|null $userVoteId
     * @return object|void id of updated entry on user_vote table
     */
    public function Update_User_Vote(int $score, int $ratingUserId=null, int $ratedUserId=null, string $ratedItem=null, int $userVoteId=null)
    {
        Logger::debug("Update_User_Vote", __FILE__);
        Logger::debug('args: ' .  implode(', ', func_get_args()));
        global $wpdb;
        $table1 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTABLE_TABLE;
        $table2 = $wpdb->prefix . CLISTADS_USER_RANKING_USER_VOTE_TABLE;
        if ($userVoteId){
            $query = $wpdb->prepare("UPDATE $table2 SET score = %d WHERE id = %d", $score, $userVoteId);
        }else{
            $query = $wpdb->prepare("UPDATE $table2 SET score = %d WHERE user_id = %d AND user_votable_id = (SELECT id FROM $table1 WHERE user_id = %d AND code = %s)", $score, $ratingUserId, $ratedUserId, $ratedItem);
        }
        Logger::debug("query: $query");
        $wpdb->query($query);
    }
    
    /**
     * Clear Database.
     *
     * @param constant $table_constant
     * @return void
     */
    public function clear_db($table_constant)
    {
        global $wpdb;
        $table = $wpdb->prefix . $table_constant;
        $query = $wpdb->prepare("TRUNCATE $table");
        $wpdb->query($query);
    }

    /**
     * validate foreign key for table conlumn.
     *
     * @param constant $table
     * @param string $column
     * @param constant $tableReferred
     * @param string $columnReferred
     * @return Boolean Result of validation.
     */
    public function validate_foreign_key($table, string $column, $tableReferred, string $columnReferred)
    {
        global $wpdb;
        $table = $wpdb->prefix . $table;
        $table_referred = $wpdb->prefix . $tableReferred;
        return  $wpdb->get_var("SELECT COUNT(*) FROM $table WHERE %s not in (SELECT %s from $table_referred WHERE id > 0)", $column, $columnReferred) > 0? False : True;
    }


}
