<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       clistads.com
 * @since      1.0.0
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/admin
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Ranking_Admin
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/clistads-user-ranking-admin.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/clistads-user-ranking-admin.js', array('jquery'), $this->version, false);
	}

	public function add_menu_pages()
	{
		add_menu_page('ClistAds User Ranking', 'Clistads User Ranking', 'manage_options', 'clistads/clistads-user-ranking.php', function () {
		});
		add_submenu_page( 'clistads/clistads-user-ranking.php', 'Unit Tests', 'Unit Tests', 'manage_options', 'clistads/clistads-user-ranking/unit-tests.php', [$this, 'unit_tests']); 
		add_submenu_page( 'clistads/clistads-user-ranking.php', 'Refactored Unit Tests', 'Refactored Unit Tests', 'manage_options', 'clistads/clistads-user-ranking/refactored-unit-tests.php', [$this, 'refactored_unit_tests']); 

	}

	public function unit_tests(){
		$userId = wp_create_user( 'user1', 'user1', 'user1' );
		$rating = GetUserRating($userId);
		echo "<h1>Create a new User</h1>";
		echo "<h2> new user created with id: $userId";
		if($rating == CLISTADS_USER_RANKING_DEFAULT_VOTE_SCORE){
			echo "<h3>SUCCESS: New User default score is at correct value: $rating </h3>";
		} else {
			echo "<h3>FAIL: New User default score is at incorrect value: $rating, should be: ". CLISTADS_USER_RANKING_DEFAULT_VOTE_SCORE . "</h3>";
		}

		echo "<h2>User Votes for themself a 10</h2>";
		$newRating = RateUser($userId, $userId, 10);
		if($newRating > $rating){
			echo "<h3>SUCCESS: User increased their score from $rating to $newRating";
		}
		else {
			echo "<h3>FAIL: User changed their score from $rating to $newRating";
		}

		echo "<h2>User Votes for themself a 10 again</h2>";
		$subsequentRating = RateUser($userId, $userId, 10);
		if($subsequentRating == $newRating){
			echo "<h3>SUCCESS: User was not able to increase their score it stayed the same at: $subsequentRating";
		}
		else {
			echo "<h3>FAIL: User changed their score from $newRating to $subsequentRating";
		}

		echo "<h1>Create another User</h1>";
		$userId2 = wp_create_user( 'user2', 'user2', 'user2' );
		echo "<h2> new user created with id: $userId";
		echo "<h2>New User Votes for first user a 10</h2>";
		$rating = GetUserRating($userId);
		$newRating = RateUser($userId, $userId2, 10);
		if($newRating > $rating){
			echo "<h3>SUCCESS: New User increased first users score from $rating to $newRating";
		}
		else {
			echo "<h3>FAIL: New User changed first users score from $rating to $newRating";
		}

		echo "<h2>New User changes mind and then votes a 1 </h2>";
		$rating = GetUserRating($userId);
		$newRating = RateUser($userId, $userId2, 1);
		if($newRating < $rating){
			echo "<h3>SUCCESS: New User decreased first users score from $rating to $newRating";
		}
		else {
			echo "<h3>FAIL: New User changed first users score from $rating to $newRating";
		}

		echo "<h2>New User votes them a 1 again</h2>";
		$rating = GetUserRating($userId);
		$subsequentNewRating = RateUser($userId, $userId2, 1);
		if($subsequentNewRating == $rating){
			echo "<h3>SUCCESS: User was not able to decrease their score it stayed the same at: $subsequentRating";
		}
		else {
			echo "<h3>FAIL: User changed their score from $rating to $subsequentNewRating";
		}

		wp_delete_user($userId);
		wp_delete_user($userId2);
	}

	public function refactored_unit_tests(){
		$dbModel = new Clistads_User_Ranking_Database_Model();
		$dbModel->clear_db(CLISTADS_USER_RANKING_USER_VOTE_TABLE);
		$dbModel->clear_db(CLISTADS_USER_RANKING_USER_VOTABLE_TABLE);

		$ratedUserId = 123;
		$ratedItem1 = "test1";
		$ratedItem2 = "test2";
		$userVotableId1 = $dbModel->Insert_user_votable($ratedUserId, $ratedItem1);
		$userVotableId2 = $dbModel->Insert_user_votable($ratedUserId, $ratedItem2);
		$ratingUserId1 = 321;
		$ratingUserId2 = 322;
		$score1 = 10;
		// insert with user_votable_id
		$user_vote_id = $dbModel->Insert_user_vote($ratingUserId1, $score1, null, null, $userVotableId1);
		$result = $dbModel->Query_User_Vote(null, null, null, $user_vote_id);
		$this-> assert($result[0]->score, $score1, "user inserted correct score");
		$score2 = 7;
		// insert with rated_user_id and code
		$dbModel->Insert_user_vote($ratingUserId2, $score2, $ratedUserId, $ratedItem1);
		$result = $dbModel->Query_User_Vote($ratingUserId2, $ratedUserId, $ratedItem1);
		$this-> assert($result[0]->score, $score2, "user inserted correct score");
		// update previous vote score
		$dbModel->Update_User_Vote($score1, null, null, null, $result[0]->id);
		$result = $dbModel->Query_User_Vote($ratingUserId2, $ratedUserId, $ratedItem1);
		$this-> assert($result[0]->score, $score1, "user inserted correct score");
		// get all votes for rated item
		$scores_array = $dbModel->Query_User_Vote(null, $ratedUserId, $ratedItem1);
		$this-> assert(count($scores_array), 2, "2 scores are noted");
		// get all votes for rated user
		$user_vote_id = $dbModel->Insert_user_vote($ratingUserId1, $score2, null, null, $userVotableId2);
		$scores_array = $dbModel->Query_User_Vote(null, $ratedUserId);
		$this-> assert(count($scores_array), 3, "3 scores are noted");

		// ensure user_votable_id is referred properly
		$this-> assert($dbModel->validate_foreign_key(CLISTADS_USER_RANKING_USER_VOTE_TABLE, "user_votable_id", CLISTADS_USER_RANKING_USER_VOTABLE_TABLE, "id"), True, "user_votable_id is referring to user_votable_table ids");
		$dbModel->validate_foreign_key(CLISTADS_USER_RANKING_USER_VOTE_TABLE, "user_votable_id", CLISTADS_USER_RANKING_USER_VOTABLE_TABLE, "id");
	}

	private function assert($left, $right, $message){
		if($left == $right){
			echo "<h3>SUCCESS: $message";
		}
		else {
			echo "<h3>FAIL: x NOT - $message";
		}
	}
	
}
