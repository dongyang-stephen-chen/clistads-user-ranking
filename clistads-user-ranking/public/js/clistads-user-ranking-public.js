(function($) {
    'use strict';
    var rateUser = async(ratedUser, rating) => {
        console.group(`rate User Called with params: ratedUser: ${ratedUser} rating: ${rating}`);
        await jQuery.ajax({
            url: data.rest.endpoints.rate_user,
            method: "POST",
            dataType: "json",
            timeout: data.rest.timeout,
            data: {
                ratedUser,
                rating
            },
            beforeSend: function(xhr) {
                xhr.setRequestHeader('X-WP-Nonce', data.rest.nonce); // use authentication nonce
            }
        }).done(function(results) {
            console.log('Done:')
            console.table(results)
        }).fail(function(xhr) {
            console.error('fail')
            console.table(xhr)
        }).catch(e => {
            console.error('catch')
            console.table(e)
        })
        console.groupEnd();
    }

    var numericScoreToString = (score) => {
        switch (true) {
            case score <= 2.8:
                return 'Very Poor'
                break;
            case score <= 4.6:
                return 'Poor'
                break;
            case score <= 6.4:
                return 'Fair';
                break;
            case score <= 8.2:
                return 'Good';
                break;
            case score <= 10:
                return 'Excellent';
                break;
        }
    }

    var getUserRating = async(userId) => {
        console.group(`get user rating Called with params: userId: ${userId}`);
        var theResult
        await jQuery.ajax({
            url: data.rest.endpoints.get_rating + `/${userId}`,
            method: "GET",
            dataType: "json",
            timeout: data.rest.timeout,
            data: {},
        }).done(function(results) {
            console.log('Done:')
            console.table(results)
            theResult = results.rating
        }).fail(function(xhr) {
            console.error('fail')
            console.table(xhr)
        }).catch(e => {
            console.error('catch')
            console.table(e)
        });
        console.log(`returning: ${theResult}`)
        console.groupEnd();
        return theResult
    }

    $(async function() {
        $('#userRankingComponent').barrating(() => {
            var theRating = await getUserRating($('#userRankingComponent').data('userid'))
            $("#currentRatingValue").text(numericScoreToString(theRating))
            return {
                theme: 'bars-movie',
                initialRating: theRating,
                onSelect: async function(value, text, event) {
                    var userId = $('#userRankingComponent').data('userid')
                    console.group(`user rating on select Called with params: userId: ${userId} value: ${value}`);
                    if (typeof(event) !== 'undefined') {
                        await rateUser(userId, value)
                        var theRating = await getUserRating($('#userRankingComponent').data('userid'))
                        console.log(`current rating is now ${theRating}`)
                        $("#currentRatingValue").text(numericScoreToString(theRating))
                        console.groupEnd()
                    } else {
                        // rating was selected programmatically
                        // by calling `set` method
                    }
                }
            }
        });
    })


})(jQuery);