<?php

/**
 * Rate the User
 *
 * @param Int $ratedUserId
 * @param Int $ratingUserId
 * @param int $rating
 * @return void
 */
function RateUser(Int $ratedUserId, Int $ratingUserId, int $rating)
{
    Logger::debug("RateUser", __FILE__);
    Logger::debug('args: ' .  implode(', ', func_get_args()));
    $rd_args = array(
        'author' => $ratingUserId,
        'post_type' => CLISTADS_USER_RANKING_POST_TYPE,
        'meta_query' => array(
            array(
                'key' => CLISTADS_USER_RANKING_META_KEY_RATED_USER,
                'value' => $ratedUserId,
                'compare' => '=',
            )
        )
    );
    $rd_query = new WP_Query($rd_args);
    $ID = 0; // If ID is 0 it will create a new post instead of update it
    if ($rd_query->have_posts()) {
        while ($rd_query->have_posts()) {
            $rd_query->the_post();
            $ID = get_the_ID();
        }
    } else {
        wp_reset_postdata();
        $postId = wp_insert_post([
            'ID' => $ID,
            'post_author' => $ratingUserId,
            'post_type' => CLISTADS_USER_RANKING_POST_TYPE,
            "post_status" => "publish",
            "post_title" => 'userRating'
        ]);
        $ID = $postId;
    }
    update_post_meta($ID, CLISTADS_USER_RANKING_META_KEY_RATED_USER_VALUE, $rating);
    update_post_meta($ID, CLISTADS_USER_RANKING_META_KEY_RATED_USER, $ratedUserId);

    $userRating = GetUserRating($ratedUserId);
    if ($userRating < 1.8 || CLISTADS_USER_RANKING_USER_FORCE_VERY_LOW_SCORE_ON == $ratedUserId) {
        VeryPoorUserWarning($ratedUserId);
        return;
    }
    if ($userRating <= 2.6 || CLISTADS_USER_RANKING_USER_FORCE_LOW_SCORE_ON == $ratedUserId) {
        PoorUserWarning($ratedUserId);
        return;
    }
    return $userRating;
}

/**
 * Get the user rating, returns 0 if no ratings found.
 *
 * @param integer $userId
 * @param bool $debug
 * @return void
 */
function GetUserRating(int $userId)
{
    Logger::debug("GetUserRating", __FILE__);
    Logger::debug('args: ' .  implode(', ', func_get_args()));
    $rd_args = array(
        'post_type' => CLISTADS_USER_RANKING_POST_TYPE,
        'meta_query' => array(
            array(
                'key' => CLISTADS_USER_RANKING_META_KEY_RATED_USER,
                'value' => $userId,
                'compare' => '=',
            )
        )
    );
    $rd_query = new WP_Query($rd_args);
    if ($rd_query->have_posts()) {
        Logger::Debug('the query has posts');
        $allKeys = [];
        while ($rd_query->have_posts()) {
            $rd_query->the_post();
            $postmetas = get_post_meta(get_the_ID());
            if (count($postmetas) > 0) {
                // get all the meta values that has the rating value
                $filteredMetaKeys = array_filter($postmetas, function ($k) {
                    return $k == CLISTADS_USER_RANKING_META_KEY_RATED_USER_VALUE;
                }, ARRAY_FILTER_USE_KEY);
                // turn the array of string values into just a integer
                $filteredMetaKeys = array_map(function ($v) {
                    return (int)$v[0];
                }, array_values($filteredMetaKeys));
                $allKeys = array_merge($allKeys, $filteredMetaKeys);
            }
        }
        $testWeight = get_user_meta($userId, CLISTADS_USER_RANKING_USER_META_SCORE_WEIGHT, true);
        $userWeight = $testWeight == false ? CLISTADS_USER_RANKING_DEFAULT_VOTE_COUNT : $testWeight;
        $numerator = array_sum($allKeys) + ($userWeight * CLISTADS_USER_RANKING_DEFAULT_VOTE_SCORE);
        $denominator = count($allKeys) + $userWeight;
        return $numerator / $denominator;
    } else {
        Logger::Debug('the query has no posts');
        return CLISTADS_USER_RANKING_DEFAULT_VOTE_SCORE;
    }
}
