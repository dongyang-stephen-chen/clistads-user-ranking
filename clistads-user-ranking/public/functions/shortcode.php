<?php

function UserRankingShortcode($atts = [], $content = null, $tag = '')
{
    ob_start();
?>
    <div>
        <span>Average Score: </span><span id="currentRatingValue"></span>
        <select id="userRankingComponent" data-userid="<?php echo $atts['userid']; ?>">
            <option value="1">Very Poor</option>
            <option value="2">Very Poor</option>
            <option value="3">Poor</option>
            <option value="4">Poor</option>
            <option value="5">Fair</option>
            <option value="6">Fair</option>
            <option value="7">Good</option>
            <option value="8">Good</option>
            <option value="9">Excellent</option>
            <option value="10">Excellent</option>
        </select>
    </div>

<?php
    return ob_get_clean();
}

add_shortcode('ClistadsUserRanking', 'UserRankingShortcode');
