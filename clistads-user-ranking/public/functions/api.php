<?php

function GetUserRatingApi(WP_REST_Request $request)
{
    Logger::debug('GetUserRatingApi', __FILE__, __LINE__);
    Logger::debug('args: ' .  implode(', ', func_get_args()));
    $rated_user_id = $request['rated_user'];
    $rating = GetUserRating($rated_user_id);
    $x = new stdClass();
    $x->rating = $rating;
    $response = new WP_REST_Response($x);
    $response->set_status(200);

    return $response;
}

function PostUserRatingApi(WP_REST_Request $request)
{
    $ratingUser = get_current_user_id();
    $params =  $request->get_params();
    $ratedUser = $params['ratedUser'];
    $rating =  $params['rating'];
    RateUser($ratedUser, $ratingUser, $rating);
    $response = new WP_REST_Response(new stdClass());
    $response->set_status(200);

    return $response;
}
