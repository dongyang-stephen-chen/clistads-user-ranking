<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       clistads.com
 * @since      1.0.0
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Clistads_User_Ranking
 * @subpackage Clistads_User_Ranking/public
 * @author     Dongyang Chen <dongyang.stephen.chen@gmail.com>
 */
class Clistads_User_Ranking_Public
{

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct($plugin_name, $version)
	{

		$this->plugin_name = $plugin_name;
		$this->version = $version;
	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/clistads-user-ranking-public.css', array(), $this->version, 'all');
		wp_enqueue_style('bars-movie', plugin_dir_url(__FILE__) . 'css/bars-movie.css', array(), $this->version, 'all');
	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts()
	{

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Clistads_User_Ranking_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Clistads_User_Ranking_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_register_script('clistads-user-ranking-public-js', plugin_dir_url(__FILE__) . 'js/clistads-user-ranking-public.js', ['jquery', 'bar-rating']);
		wp_localize_script('clistads-user-ranking-public-js', 'data', [
			'rest' => [
				'endpoints' => [
					'rate_user'       => esc_url_raw(rest_url('clistads-user-ranking/v1/`rate-user`')),
					'get_rating'       => esc_url_raw(rest_url('clistads-user-ranking/v1/`user-rating`')),
				],
				'timeout'   => (int) apply_filters("my_plugin_rest_timeout", 3000), // don't set this shit too low
				'nonce'     => wp_create_nonce('wp_rest'), // wp_rest is important to be used but nowhere will tell you this
			],
		]);
		wp_enqueue_script('clistads-user-ranking-public-js');

		wp_enqueue_script('bar-rating', plugin_dir_url(__FILE__) . 'js/jquery.barrating.min.js', array('jquery'), $this->version, false);
	}

	/**
	 * Register the custom post type
	 *
	 * @return void
	 */
	public function RegisterPostType()
	{
		register_post_type(
			CLISTADS_USER_RANKING_POST_TYPE,
			array(
				'labels' => array(
					'name' => _('User Rankings'),
					'singular_name' => _('User Ranking')
				),
				'public' => false,
				'has_archive' => false,
				'delete_with_user' => true,
				'show_in_rest' => false
			)
		);
	}

	public function RegisterApiEndpoint()
	{
		register_rest_route('clistads-user-ranking/v1', 'user-rating/(?P<rated_user>\d+)', array(
			'methods'  => 'GET',
			'callback' => 'GetUserRatingApi',
			'permission_callback' => function (WP_REST_Request $request) {
				return true;
			},
		));

		register_rest_route('clistads-user-ranking/v1', 'rate-user', array(
			'methods'  => 'POST',
			'callback' => 'PostUserRatingApi',
			'permission_callback' => function (WP_REST_Request $request) {
				return true;
			},
		));
	}

	public function RegisterUserRestrictedView()
	{
		Logger::debug('RegisterUserRestrictedView', __FILE__);
		$userId = get_current_user_id();
		$rating = GetUserRating($userId);
		if ($rating <= 2 || $userId == CLISTADS_USER_RANKING_USER_FORCE_VERY_LOW_SCORE_ON) {
			wp_redirect(CLISTADS_USER_RANKING_SCORE_TOO_LOW_PAGE);
		}
	}

	public function IncreaseRank($order_id)
	{
		$order = wc_get_order($order_id);
		foreach ($order->get_items() as $item_id => $item) {
			$product_id = $item->get_product_id(); // the Product id
			if ($product_id == CLISTADS_USER_RANKING_INCREASE_SCORE_PRODUCT_ID) {
				$userId = get_current_user_id();
				$testWeight = get_user_meta($userId, CLISTADS_USER_RANKING_USER_META_SCORE_WEIGHT, true);
				$userWeight = $testWeight == false ? CLISTADS_USER_RANKING_DEFAULT_VOTE_COUNT : $testWeight;
				$userWeight += CLISTADS_USER_RANKING_DEFAULT_VOTE_COUNT;
				update_user_meta($userId, CLISTADS_USER_RANKING_USER_META_SCORE_WEIGHT, $userWeight);
			}
		}
	}
}
